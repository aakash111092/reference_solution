package com.jhu.driversed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Gavi on 3/10/16.
 */
public class StatisticsFrag extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        style();

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.statistics_content, container, false);
    }

    /** Style the view. */
    private void style() {
        getActivity().setTitle("Statistics");
    }

}
