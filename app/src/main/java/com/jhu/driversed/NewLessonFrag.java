//TODO: Return to screen while timer is running, clear button turns to start

package com.jhu.driversed;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Gavi on 3/10/16.
 */
public class NewLessonFrag extends Fragment {

    /************************************ VARIABLES ******************************************/

    private Button startButton;
    private Button stopButton;
    private TextView dateField;
    private TextView hoursField;
    private RadioGroup lightRadioGroup;
    private View view;

    private boolean started;    //flag if start button pressed
    private boolean stopped;    //flag if stop button pressed
    private long startTime;


    /************************************ LISTENERS ******************************************/

    View.OnClickListener startListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!started) { //start

                // set the dateTextView to current date
                Date currDate = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
                dateField.setText(formatter.format(currDate));

                //store the current time
                startTime = currDate.getTime();

                startButton.setText("Clear");
                started = true;
                hoursField.setText("Timing lesson...");

            } else {    //clear
                startButton.setText("Start");
                started = false;
                clearLesson();
            }
        }
    };

    View.OnClickListener stopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!stopped) { //stop
                stopButton.setText("Save");
                stopped = true;

                //calculate total hours elapsed and set hoursTextView
                Date currDate = new Date();
                long endTime = currDate.getTime();
                long difference = endTime - startTime;
                long minutes = (difference / (1000 * 60)) % 60;
                double total = ((double) minutes / 60.0);

                //round the value to two places after decimal
                total = Math.round(total * 100);
                total = total / 100;

                //set the hoursTextView
                hoursField.setText(Double.toString(total));

            } else {    //save
                stopButton.setText("Stop");
                stopped = false;
                started = false;
                clearLesson();
                startButton.setText("Start");

                //snackbar alert for saved lesson
                Snackbar.make(view, "Lesson saved!", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();

            }
        }
    };

    RadioGroup.OnCheckedChangeListener checkedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // handle radio button check
        }
    };

    /************************************ LIFE CYCLE ******************************************/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.new_lesson_content, container, false);

        startButton = (Button) view.findViewById(R.id.start_button);
        stopButton = (Button) view.findViewById(R.id.stop_button);
        dateField = (TextView) view.findViewById(R.id.dateField);
        hoursField = (TextView) view.findViewById(R.id.hoursField);
        lightRadioGroup = (RadioGroup) view.findViewById(R.id.lightRadioGroup);

        started = false;
        stopped = false;

        setListeners();
        style();

        Modules mods = new Modules();
        mods.setupSpinners(view);

        return view;
    }

    /** Assign event listeners. */
    private void setListeners() {
        startButton.setOnClickListener(startListener);
        stopButton.setOnClickListener(stopListener);
        lightRadioGroup.setOnCheckedChangeListener(checkedListener);
    }

    /** Style the view. */
    private void style() {
        dateField.setFocusable(false);
        hoursField.setFocusable(false);
        getActivity().setTitle("New Lesson");
    }

    /** Clear the currently recorded lesson. */
    private void clearLesson() {
        dateField.setText("");
        hoursField.setText("");
    }

}
