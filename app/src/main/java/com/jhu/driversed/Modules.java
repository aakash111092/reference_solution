package com.jhu.driversed;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Created by Gavi on 3/23/16.
 */
public class Modules {

    /** Setup spinner data. */
    public void setupSpinners(View view) {
        SpinnerData data = new SpinnerData();
        ArrayAdapter<String> lessonTypeAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_item, SpinnerData.lessonTypeData);
        ArrayAdapter<String> weatherAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_item, SpinnerData.weatherData);
        lessonTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        weatherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        ((Spinner)view.findViewById(R.id.lessonSpinner)).setAdapter(lessonTypeAdapter);
        ((Spinner)view.findViewById(R.id.weatherSpinner)).setAdapter(weatherAdapter);
    }

}
