package com.jhu.driversed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

/**
 * Created by Gavi on 3/10/16.
 */
public class DrivingLogFrag extends Fragment {

    /************************************ VARIABLES ******************************************/

    private View view;

    /************************************ LIFE CYCLE ******************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.driving_log_content, container, false);

        style();
        setupListData();

        // Inflate the layout for this fragment
        return view;
    }

    /************************************ METHODS ******************************************/


    /** Setup list view data and long click action. */
    private void setupListData() {

        //setup data
        final DrivingLogData data = new DrivingLogData();
        final ListView listView = (ListView) view.findViewById(R.id.drivingLogListView);
        SimpleAdapter adapter = new SimpleAdapter(
                this.getContext(), DrivingLogData.drivingLogData,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        listView.setAdapter(adapter);

        //set long click action
        listView.setLongClickable(true);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {

                //extract hours
                String prefix = "Hours You Drove: ";
                String hours = DrivingLogData.drivingLogData.get(pos).get("title");
                hours = hours.substring(prefix.length(), hours.length());

                //extract date
                String date = DrivingLogData.drivingLogData.get(pos).get("subtitle");

                //show popup to edit log item
                EditLogDialogue popUp = new EditLogDialogue(getActivity());
                popUp.setHours(hours);
                popUp.setDate(date);
                popUp.show();
                return true;
            }
        });
    }


    /** Style the view. */
    private void style() {
        getActivity().setTitle("Driving Log");  //set title
    }

}
