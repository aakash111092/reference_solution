package com.jhu.driversed;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.HashMap;

public class SettingsActivity extends AppCompatActivity {

    private final static int MAX_HOURS = 23;
    private final static int MAX_MINUTES = 59;
    private final static int DEFAULT_MINUTES = 10;
    private static final int MINUTES_PER_HOUR = 60;

    //ids of text views
    private final static int[] ids = {
            R.id.totalReq, R.id.dayReq, R.id.nightReq, R.id.residentialReq, R.id.commercialReq,
            R.id.highwayReq, R.id.clearReq, R.id.rainyReq, R.id.snowIceReq
    };

    //default values for text views. sets all values to 10 and sums the total
    public static final HashMap<Integer, Integer> defaultVals = new HashMap(){
        {
            for (int id: ids) {
                put(id, DEFAULT_MINUTES * MINUTES_PER_HOUR);
            }
            put(R.id.totalReq, (ids.length - 1) * (DEFAULT_MINUTES * MINUTES_PER_HOUR));
        }
    };

    private SharedPreferences myPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        //set back button
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        setTitle("Settings");
        setupUI();
    }

    private void setupUI() {

         //setup listener and values for each specific text view
         for (int id: ids) {
             final TextView hoursTextView =  (TextView) findViewById(id);

             //set listener
             View.OnClickListener listener = new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     showTimePicker(hoursTextView);
                 }
             };

             //do not set listener for total hours view
             if (id != R.id.totalReq) {
                 hoursTextView.setOnClickListener(listener);
             }

             //disable focus
             hoursTextView.setFocusable(false);

             //retrieve data
             int total = myPrefs.getInt(Integer.toString(id), -1);

             //if data not yet stored, retrieve from default vals
             if (total == -1) {
                 setTextView(hoursTextView, defaultVals.get(id));

                 //save new value in prefs
                 SharedPreferences.Editor peditor = myPrefs.edit();
                 peditor.putInt(Integer.toString(id), defaultVals.get(id));
                 peditor.commit();

             } else {
                 setTextView(hoursTextView, total);
             }
         }
    }

    //Bring up a time picker dialogue
    private void showTimePicker(final TextView hrs) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.driving_time_picker, null);

        //setup dialogue box
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setTitle(R.string.setDrivingTime);

        //setup picker values
        final NumberPicker hourPicker = (NumberPicker) view.findViewById(R.id.hoursPicker);
        final NumberPicker minutePicker = (NumberPicker) view.findViewById(R.id.minutesPicker);
        hourPicker.setMaxValue(MAX_HOURS);
        minutePicker.setMaxValue(MAX_MINUTES);
//        hourPicker.setValue(Integer.parseInt(hrs.getText().toString().substring(0, 2)));
//        minutePicker.setValue(Integer.parseInt(hrs.getText().toString().substring(4, 6)));
        String[] times = hrs.getText().toString().split(":", 2);
        hourPicker.setValue(Integer.parseInt(times[0]));
        minutePicker.setValue(Integer.parseInt(times[1]));

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { }
        });
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //calculate the new total hours required
                int newVal = hourPicker.getValue() * MINUTES_PER_HOUR + minutePicker.getValue();
                int oldVal = myPrefs.getInt(Integer.toString(hrs.getId()), -1);
                int oldTotal = myPrefs.getInt(Integer.toString(R.id.totalReq), -1);
                int newTotal = oldTotal - oldVal + newVal;

                //store total in prefs
                SharedPreferences.Editor peditor = myPrefs.edit();
                peditor.putInt(Integer.toString(R.id.totalReq), newTotal);
                peditor.putInt(Integer.toString(hrs.getId()), newVal);
                peditor.commit();

                //set the changed text views
                setTextView(hrs, hourPicker.getValue() * MINUTES_PER_HOUR + minutePicker.getValue());
                setTextView((TextView) findViewById(R.id.totalReq), newTotal);

            }
        });
        builder.show();
    }

    /**
     * Set the hours and minutes for the text view
     * @param hoursTextView view to set
     * @param totalTime total time in minutes
     */
    private void setTextView(TextView hoursTextView, int totalTime) {
        String hours = String.format("%02d", totalTime / MINUTES_PER_HOUR);
        String minutes = String.format("%02d", totalTime % MINUTES_PER_HOUR);
        hoursTextView.setText(hours + ":" + minutes);
    }
}
