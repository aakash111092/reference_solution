package com.jhu.driversed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Gavi on 3/23/16. Shows a diologue box to edit log item
 */
public class EditLogDialogue {

    private AlertDialog.Builder builder;
    private View dialoglayout;

    public EditLogDialogue(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();
        dialoglayout = inflater.inflate(R.layout.edit_log, null);

        //setup dialogue box
        builder = new AlertDialog.Builder(activity);
        builder.setView(dialoglayout);
        builder.setTitle(R.string.editLog);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                System.out.println("Canceled");
            }
        });
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                System.out.println("Saved");
            }
        });

        //setup spinners
        Modules mods = new Modules();
        mods.setupSpinners(dialoglayout);

    }

    public void setHours(String h) {
        ((TextView) dialoglayout.findViewById(R.id.hoursTextView)).setText(h);
    }

    public void setDate(String d) {
        ((TextView) dialoglayout.findViewById(R.id.dateTextView)).setText(d);
        System.out.println(d);
    }


    public void show() {
        builder.show();
    }
}
