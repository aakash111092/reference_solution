package com.jhu.driversed;

/**
 * Created by Gavi on 3/10/16.
 */
public class SpinnerData {

    public static String[] lessonTypeData = {
            "Residential", "Commercial", "Highway"
    };

    public static String[] weatherData = {
            "Clear", "Raining", "Snow/Ice"
    };

}
