package com.jhu.driversed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gavi on 3/22/16.
 */


public class DrivingLogData {

    public static final String PREFIX = "Hours You Drove: ";

    public static String[][] cellData = {
            {PREFIX + "3.12", "Tuesday 2/23/18"},
            {PREFIX + "4.21", "Wednesday 2/24/18"},
            {PREFIX + "2.11", "Thursday 2/25/18"},
            {PREFIX + "1.41", "Thursday 2/25/18"}
    };

    public static ArrayList<Map<String, String>> drivingLogData;

    public DrivingLogData() {
        drivingLogData = new ArrayList<>();
        for (int r = 0; r < cellData.length; r++) {
            Map<String, String> datum = new HashMap<>(2);
            datum.put("title", cellData[r][0]);
            datum.put("subtitle", cellData[r][1]);
            drivingLogData.add(datum);
        }
    }
}
